import sys

def sum_cycles(filename):
    total_cycles = 0
    with open(filename, 'r') as file:
        for line in file:
            parts = line.split('|')
            if len(parts) > 1:
                try:
                    cycles = float(parts[1].strip())
                    total_cycles += cycles
                except ValueError:
                    # Handle lines where the cycle count is not a float
                    continue

    return total_cycles

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python sum_cycles.py <osaca_output_file>")
        sys.exit(1)

    filename = sys.argv[1]
    total_cycles = sum_cycles(filename)
    print("Total Cycles: {}".format(total_cycles))

