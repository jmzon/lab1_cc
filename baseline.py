import json

def load_product_of_sums (data):
    
    def product_of_sums (a ,n):
        o = []
        for j in range (n):
            product = 1
            for sum_entry in data['expression']:
                #extract the indices and compute the sum
                indices = [int(s.strip()[2:-1]) for s in sum_entry]
                sum_total = sum(a[(j + i)% len(a)] for i in indices)
                product *= sum_total
            o.append (product)
        return o 

    return product_of_sums

# Example usage
with open("expression.json", 'r') as file:
    expression = json.load(file)

n = 5
a = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]

function = load_product_of_sums(expression)

result = function(a, n)

for i in result:
    print(i)