import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def generate_c_function(expression, c_file_path):
    function_code = "#include <stdio.h>\n"
    function_code += "#include <xmmintrin.h>  // SSE intrinsics\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  int vec_n = n / 4;  // Number of vectorized operations\n"
    function_code += "  for (int i = 0; i < vec_n; ++i) {\n"
    function_code += "    __m128 va = _mm_loadu_ps(&a[i*4]);  // Load 4 floats\n"
    # Dynamically generate SIMD operations based on the expression
    terms = ["_mm_add_ps(va, va)"]  # Placeholder for actual SIMD operation
    for term in terms:
        function_code += f"    va = {term};\n"
    function_code += "    _mm_storeu_ps(&o[i*4], va);  // Store the result\n"
    function_code += "  }\n"
    # Handle remaining elements if n % 4 != 0
    function_code += "  for (int i = vec_n * 4; i < n; ++i) {\n"
    function_code += "    o[i] = a[i];  // Placeholder for scalar operation\n"
    function_code += "  }\n"
    function_code += "}\n\n"
    function_code += "int main(int argc, char *argv[]) {\n"
    function_code += "  int n = 8;  // Ensure n is a multiple of 4 for simplicity\n"
    function_code += "  float a[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};\n"
    function_code += "  float o[8];\n"
    function_code += "  compute(a, o, n);\n\n"
    function_code += "  for (int i = 0; i < n; i++) {\n"
    function_code += "    printf(\"%.2f\\n\", o[i]);\n"
    function_code += "  }\n"
    function_code += "  return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)

def main():
    json_file_path = 'expression_gen.json'
    c_file_path = 'simd_function.c'  # Output C file
    expression = read_expression_from_json(json_file_path)
    generate_c_function(expression, c_file_path)
    print("C code with SIMD instructions generated and written to {}".format(c_file_path))

if __name__ == "__main__":
    main()
