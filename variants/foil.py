import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def generate_c_function(expression, c_file_path):
    function_code = "#include <stdio.h>\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  for (int i = 0; i < n; ++i) {\n"
    function_code += "      float precomputed_sums[] = {"
    precomputed_terms = ["0"]  # Placeholder for actual precomputation logic
    function_code += ", ".join(precomputed_terms)
    function_code += "};\n"
    # Use precomputed sums in the calculation
    terms = ["precomputed_sums[0]"]  # Simplified example, replace with actual logic
    for sum_terms in expression[1:]:  # Assuming the first sum is precomputed
        terms.append("(" + " + ".join(sum_terms).replace("[", "[(i + ").replace("]", ") % n]") + ")")
    function_code += " * ".join(terms) + ";\n"
    function_code += "  }\n"
    function_code += "}\n"
    function_code += "int main(int argc, char *argv[]) {\n"
    function_code += "    int n = 5;\n"
    function_code += "    float a[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};\n"
    function_code += "    float o[5];\n"
    function_code += "    compute(a, o, n);\n"
    function_code += "\n"
    function_code += "    for (int i = 0; i < n; i++) {\n"
    function_code += "      printf(\"%.1f\\n\", o[i]);\n"
    function_code += "    }\n"
    function_code += "    return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)



def main():
    json_file_path = 'expression_gen.json'  
    c_file_path = 'foil_function.c'  # Output C file
    expression = read_expression_from_json(json_file_path)
    c_function_code = generate_c_function(expression, c_file_path)
    print("C code generated and written to {}".format(c_file_path))


if __name__ == "__main__":
    main()