import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def generate_pipelined_c_function(expression, c_file_path):
    function_code = "#include <stdio.h>\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  float prev_val = 0.0;\n" 
    function_code += "  if (n > 0) prev_val = a[0];\n"  
    function_code += "  for (int i = 1; i < n; ++i) {\n"  
    function_code += "    asm volatile (\n"
    function_code += "        \"movss %[src], %%xmm0;\\n\\t\"\n"
    function_code += "        \"movss %[prev], %%xmm1;\\n\\t\"  // Load prev_val into xmm1\n"
    function_code += "        \"addss %%xmm1, %%xmm0;\\n\\t\"    // Add xmm1 to xmm0\n"
    function_code += "        \"movss %%xmm0, %[dest];\\n\\t\"\n"
    function_code += "        \"movss %%xmm0, %[prev];\\n\"\n"
    function_code += "        : [dest] \"=m\" (o[i]), [prev] \"+m\" (prev_val)  // Output and input/output operands\n"
    function_code += "        : [src] \"m\" (a[i])                              // Input operand\n"
    function_code += "        : \"%xmm0\", \"%xmm1\"                           // Clobbered registers\n"
    function_code += "    );\n"
    
    function_code += "  }\n"
    function_code += "}\n"
    function_code += "int main(int argc, char *argv[]) {\n"
    function_code += "    int n = 5;\n"
    function_code += "    float a[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};\n"
    function_code += "    float o[5] = {0};\n"
    function_code += "    compute(a, o, n);\n"
    function_code += "\n"
    function_code += "    for (int i = 0; i < n; i++) {\n"
    function_code += "      printf(\"%.1f\\n\", o[i]);\n"
    function_code += "    }\n"
    function_code += "    return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)

def main():
    json_file_path = 'expression_gen.json'
    c_file_path = 'pipeline_function.c'  # Output C file with pipelined compute
    expression = read_expression_from_json(json_file_path)
    generate_pipelined_c_function(expression, c_file_path)
    print("C code generated and written to {}".format(c_file_path))

if __name__ == "__main__":
    main()
