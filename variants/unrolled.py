import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def generate_c_function_unrolled(expression, c_file_path):
    unroll_factor = 8
    function_code = "#include <stdio.h>\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  int i = 0;\n"
    function_code += "  for (; i <= n - {}; i += {}) {{\n".format(unroll_factor, unroll_factor)
    for j in range(unroll_factor):
        function_code += "    o[i + {}] = ".format(j) + " * ".join(["(" + " + ".join(sum_terms).replace("[", "[(i + " + str(j) + " + ").replace("]", ") % sizeof(a)]") + ")" for sum_terms in expression]) + ";\n"
    function_code += "  }\n"
    function_code += "  for (; i < n; ++i) {\n"
    function_code += "    o[i] = " + " * ".join(["(" + " + ".join(sum_terms).replace("[", "[(i + ").replace("]", ") % sizeof(a)]") + ")" for sum_terms in expression]) + ";\n"
    function_code += "  }\n"
    function_code += "}\n"

    function_code += "int main() {\n"
    function_code += "    int n = 5;\n"
    function_code += "    float a[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};\n"
    function_code += "    float o[5];\n"
    function_code += "    compute(a, o, n);\n"
    function_code += "\n"
    function_code += "    for (int i = 0; i < n; i++) {\n"
    function_code += "        printf(\"%.1f\\n\", o[i]);\n"
    function_code += "    }\n"
    function_code += "    return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)

def main():
    json_file_path = 'expression_gen.json'  
    c_file_path = 'unrolled_function.c'  # Output C file
    expression = read_expression_from_json(json_file_path)
    c_function_code = generate_c_function_unrolled(expression, c_file_path)
    print("C code generated and written to {}".format(c_file_path))

if __name__ == "__main__":
    main()