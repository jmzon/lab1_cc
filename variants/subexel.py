import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def optimize_expression(expression):
    # Identify common subexpressions
    subexpressions = {}
    optimized_expression = []

    for sum_terms in expression:
        optimized_sum_terms = []
        for term in sum_terms:
            # Replace the term with a unique subexpression name if not already replaced
            if term not in subexpressions:
                # Assign the current length of subexpressions as the unique index
                subexpressions[term] = len(subexpressions)
            # Append the subexpression index to the optimized terms list
            optimized_sum_terms.append(subexpressions[term])
        optimized_expression.append(optimized_sum_terms)

    return subexpressions, optimized_expression

def generate_c_function(expression, c_file_path):
    subexpressions, optimized_expression = optimize_expression(expression)
    
    function_code = "#include <stdio.h>\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  float subexpressions[{}];\n".format(len(subexpressions))
    function_code += "  for (int i = 0; i < n; ++i) {\n"

    # Generate the code for computing subexpressions
    for term, index in subexpressions.items():
        # Use the index to access the subexpressions array
        function_code += "      subexpressions[{}] = {};\n".format(index, term.replace("[", "[(i + ").replace("]", ") % n]"))

    # Generate the code for computing the final expression using the subexpressions
    terms = ["(" + " + ".join(['subexpressions[{}]'.format(index) for index in sum_terms]) + ")" for sum_terms in optimized_expression]
    function_code += "      o[i] = " + " * ".join(terms) + ";\n"
    function_code += "  }\n"
    function_code += "}\n"
    function_code += "int main(int argc, char *argv[]) {\n"
    function_code += "    int n = 10;\n"
    function_code += "    float a[n];\n"
    function_code += "    for(int i = 0; i < n; i++) a[i] = i + 1;\n"
    function_code += "    float o[n];\n"
    function_code += "    compute(a, o, n);\n"
    function_code += "\n"
    function_code += "    for (int i = 0; i < n; i++) {\n"
    function_code += "      printf(\"%.1f\\n\", o[i]);\n"
    function_code += "    }\n"
    function_code += "    return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)



def main():
    json_file_path = 'expression_gen.json'  # Input JSON file
    c_file_path = 'subexel_function.c'  # Output C file
    expression = read_expression_from_json(json_file_path)
    generate_c_function(expression, c_file_path)
    print("C code generated and written to {}".format(c_file_path))

if __name__ == "__main__":
    main()
