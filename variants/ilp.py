import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def generate_ilp_c_function(expression, c_file_path):
    function_code = "#include <stdio.h>\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  for (int i = 0; i < n; ++i) {\n"
    # Start by assuming we can compute parts of the expression in parallel
    independent_computations = []
    for sum_terms in expression:
        for term in sum_terms:
            computation = term.replace("[", "[(i + ").replace("]", ") % n]") 
            independent_computations.append(f"({computation})")
    
    if independent_computations:
        function_code += "      o[i] = " + " + ".join(independent_computations) + ";\n"
    else:
        function_code += "      o[i] = 0;\n"
    
    function_code += "  }\n"
    function_code += "}\n"
    function_code += "int main(int argc, char *argv[]) {\n"
    function_code += "    int n = 5;\n"
    function_code += "    float a[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};\n"
    function_code += "    float o[5];\n"
    function_code += "    compute(a, o, n);\n"
    function_code += "\n"
    function_code += "    for (int i = 0; i < n; i++) {\n"
    function_code += "      printf(\"%.1f\\n\", o[i]);\n"
    function_code += "    }\n"
    function_code += "    return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)

def main():
    json_file_path = 'expression_gen.json'
    c_file_path = 'ilp_function.c'  # Output C file
    expression = read_expression_from_json(json_file_path)
    # Correct the function name here to match the defined function
    generate_ilp_c_function(expression, c_file_path)
    print("C code generated and written to {}".format(c_file_path))

if __name__ == "__main__":
    main()
