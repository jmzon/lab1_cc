import json

def read_expression_from_json(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data['expression']

def generate_c_function(expression, c_file_path):
    function_code = "#include <stdio.h>\n\n"
    function_code += "void compute(float *a, float *o, int n) {\n"
    function_code += "  for (int i = 0; i < n; ++i) {\n"
    
    # Starting inline assembly block
    function_code += "    asm volatile (\n"
    function_code += "        \"movss %[src], %%xmm0;\\n\\t\"  // Example: Load src to xmm0\n"
    function_code += "        \"addss %%xmm0, %%xmm1;\\n\\t\" // Example: Add xmm0 to xmm1\n"
    function_code += "        \"movss %%xmm1, %[dest];\"    // Example: Store xmm1 to dest\n"
    function_code += "        : [dest] \"=m\" (o[i])        // Output operand\n"
    function_code += "        : [src] \"m\" (a[i])          // Input operand\n"
    function_code += "        : \"%xmm0\", \"%xmm1\"        // Clobbered registers\n"
    function_code += "    );\n"
    
    function_code += "  }\n"
    function_code += "}\n"
    function_code += "int main(int argc, char *argv[]) {\n"
    function_code += "    int n = 5;\n"
    function_code += "    float a[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};\n"
    function_code += "    float o[5];\n"
    function_code += "    compute(a, o, n);\n"
    function_code += "\n"
    function_code += "    for (int i = 0; i < n; i++) {\n"
    function_code += "      printf(\"%.1f\\n\", o[i]);\n"
    function_code += "    }\n"
    function_code += "    return 0;\n"
    function_code += "}\n"

    with open(c_file_path, 'w') as file:
        file.write(function_code)




def main():
    json_file_path = 'expression_gen.json'  
    c_file_path = 'ass_function.c'  # Output C file
    expression = read_expression_from_json(json_file_path)
    c_function_code = generate_c_function(expression, c_file_path)
    print("C code generated and written to {}".format(c_file_path))


if __name__ == "__main__":
    main()