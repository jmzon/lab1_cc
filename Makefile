ITERATIONS := 100

all: run compare-files valgrind osaca_analysis

run:
	python3 gen_expression.py
	python3 ./variants/variant1.py
	gcc compute_function.c -o compute_function
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./compute_function > cout; done' \
	    2>&1 | tee c_time.txt

	python3 ./variants/unrolled.py
	gcc unrolled_function.c -o unrolled_function
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./unrolled_function > unrolled_cout; done' \
	    2>&1 | tee unrolled_c_time.txt	

	python3 ./variants/subexel.py
	gcc subexel_function.c -o subexel_function
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./subexel_function > subexel_cout; done' \
	    2>&1 | tee subexel_c_time.txt

	python3 ./variants/foil.py
	gcc foil_function.c -o foil_function
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./foil_function > foil_cout; done' \
	    2>&1 | tee foil_c_time.txt

	python3 ./variants/simd.py
	gcc simd_function.c -o simd_function
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./simd_function > simd_cout; done' \
	    2>&1 | tee simd_c_time.txt
	
	python3 ./variants/ilp.py  # Adjust the path if necessary
	gcc ilp_function.c -o ilp_function -O3
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./ilp_function > ilp_cout; done' \
	    2>&1 | tee ilp_c_time.txt

	python3 ./variants/ass.py
	gcc ass_function.c -o ass_function -O3
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./ass_function > ass_cout; done' \
	    2>&1 | tee ass_c_time.txt

	python3 ./variants/pipeline.py
	gcc pipeline_function.c -o pipeline_function -O3
	@/usr/bin/time -f "Total execution time: %e seconds" \
	    sh -c 'for i in $$(seq 1 $(ITERATIONS)); do ./pipeline_function > pipeline_cout; done' \
	    2>&1 | tee pipeline_c_time.txt

	/usr/bin/time -v python3 baseline.py > p_out.txt 2> p_time.txt

.PHONY: compare-files valgrind

compare-files:
	@if cmp -s c_out.txt p_out.txt; then \
	    echo "PASS"; \
	else \
	    echo "NOT_PASS"; \
	fi
	@if cmp -s unrolled_cout p_out.txt; then \
	    echo "UNROLLED_PASS"; \
	else \
	    echo "UNROLLED_NOT_PASS"; \
	fi
	@if cmp -s subexel_cout p_out.txt; then \
		echo "SUBEXEL_PASS"; \
	else \
	    echo "SUBEXEL_NOT_PASS"; \
	fi
	# Compare foil output
	@if cmp -s foil_cout p_out.txt; then \
	    echo "FOIL_PASS"; \
	else \
	    echo "FOIL_NOT_PASS"; \
	fi
	#	Compare simd output
	@if cmp -s simd_cout p_out.txt; then \
	    echo "SIMD_PASS"; \
	else \
	    echo "SIMD_NOT_PASS"; \
	fi
		# Compare ilp output
	@if cmp -s ilp_cout p_out.txt; then \
	    echo "ILP_PASS"; \
	else \
	    echo "ILP_NOT_PASS"; \
	fi
	@if cmp -s ass_cout p_out.txt; then \
	    echo "ASS_PASS"; \
	else \
	    echo "ASS_NOT_PASS"; \
	fi
	@if cmp -s pipeline_cout p_out.txt; then \
	    echo "PIPELINE_PASS"; \
	else \
	    echo "PIPELINE_NOT_PASS"; \
	fi
valgrind:
	@echo "Running Valgrind on compute_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./compute_function > cout
	@echo "Running Valgrind on unrolled_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./unrolled_function > unrolled_cout
	@echo "Running Valgrind on subexel_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./subexel_function > subexel_cout
	# New Valgrind check for foil_function
	@echo "Running Valgrind on foil_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./foil_function > foil_cout
	@echo "Running Valgrind on simd_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./simd_function > simd_cout
	@echo "Running Valgrind on ilp_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./ilp_function > ilp_cout
	@echo "Running Valgrind on ass_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./ass_function > ass_cout
	@echo "Running Valgrind on pipeline_function..."
	valgrind --leak-check=full --show-leak-kinds=all ./pipeline_function > pipeline_cout

osaca_analysis:
	@echo "Running OSACA analysis on compute_function..."
	gcc -S -fverbose-asm -O2 compute_function.c -o compute_function.s
	osaca --arch ZEN2 compute_function.s > osaca_output_compute.txt
	python sum_cycles.py osaca_output_compute.txt > cycles_compute.txt

	@echo "Running OSACA analysis on unrolled_function..."
	gcc -S -fverbose-asm -O2 unrolled_function.c -o unrolled_function.s
	osaca --arch ZEN2 unrolled_function.s > osaca_output_unrolled.txt
	python sum_cycles.py osaca_output_unrolled.txt > cycles_unrolled.txt

	@echo "Running OSACA analysis on subexel_function..."
	gcc -S -fverbose-asm -O2 subexel_function.c -o subexel_function.s
	osaca --arch ZEN2 subexel_function.s > osaca_output_subexel.txt
	python sum_cycles.py osaca_output_subexel.txt > cycles_subexel.txt

	@echo "Running OSACA analysis on foil_function..."
	gcc -S -fverbose-asm -O2 foil_function.c -o foil_function.s
	osaca --arch ZEN2 foil_function.s > osaca_output_foil.txt
	python sum_cycles.py osaca_output_foil.txt > cycles_foil.txt

	@echo "Running OSACA analysis on simd_function..."
	gcc -S -fverbose-asm -O2 simd_function.c -o simd_function.s
	osaca --arch ZEN2 simd_function.s > osaca_output_simd.txt
	python sum_cycles.py osaca_output_simd.txt > cycles_simd.txt

	@echo "Running OSACA analysis on ilp_function..."
	gcc -S -fverbose-asm -O2 ilp_function.c -o ilp_function.s
	osaca --arch ZEN2 ilp_function.s > osaca_output_ilp.txt
	python sum_cycles.py osaca_output_ilp.txt > cycles_ilp.txt	

	@echo "Running OSACA analysis on ass_function..."
	gcc -S -fverbose-asm -O2 ass_function.c -o ass_function.s
	sed 's/;.*//' ass_function.s | sed 's/#.*//' > ass_function_clean.s
	osaca --arch ZEN2 ass_function_clean.s > osaca_output_ass.txt
	python sum_cycles.py osaca_output_ass.txt > cycles_ass.txt

	@echo "Running OSACA analysis on pipeline_function..."
	gcc -S -fverbose-asm -O2 pipeline_function.c -o pipeline_function.s
	sed 's/;.*//' pipeline_function.s | sed 's/#.*//' > pipeline_function_clean.s
	osaca --arch ZEN2 pipeline_function_clean.s > osaca_output_pipeline.txt
	python sum_cycles.py osaca_output_pipeline.txt > cycles_pipeline.txt

	@echo "Best variant based on OSACA analysis:"
	python3 best.py
	python3 plot.py

clean:
	rm -f c_out.txt p_out.txt compute_function c_time.txt p_time.txt osaca_output.txt compute_function.s test_cases.pyc compute_function.c expression_gen.json unrolled_cout unrolled_c_time.txt unrolled_function unrolled_function.s osaca_output_unrolled.txt
	rm -f unrolled_function.c subexel_function subexel_function.s subexel_function.c subexel_cout subexel_c_time.txt osaca_output_subexel.txt cout
	rm -f foil_function foil_function.c foil_cout foil_c_time.txt osaca_output_foil.txt # Ensure cleanup for foil variant
	rm -f foil_function.s simd_c_time simd_cout simd_function osaca_output_simd osaca_output_simd.txt simd_function.c simd_function.s simd_c_time.txt
	rm -f ilp_function ilp_function.c ilp_cout ilp_c_time.txt osaca_output_ilp.txt ilp_function.s
	rm -f ass_function ass_function.c ass_cout ass_c_time.txt osaca_output_ass.txt ass_function.s ass_function_clean.s ass_function_nocomments.s ilp.cout ass.cout
	rm -f pipeline_function pipeline_function.c pipeline_cout pipeline_c_time.txt osaca_output_pipeline.txt pipeline_function.s pipeline_function_clean.s
	rm -f cycles_ass.txt cycles_compute.txt cycles_foil.txt cycles_ilp.txt cycles_pipeline.txt cycles_simd.txt cycles_subexel.txt cycles_unrolled.txt osaca_output_compute.txt osaca_cycle_counts.png
	
