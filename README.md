# lab1_CC

This lab consists of a set of programs and scripts designed to analyze and compare the performance of different variants of a computational function. The main goal is to evaluate the execution time and resource usage of each variant using various methodologies such as direct timing, comparison with a baseline, Valgrind analysis for memory leaks, and OSACA analysis for cycle counts.

Requirements
Python 3.x
GCC (GNU Compiler Collection)
Valgrind
OSACA (Optimizing Static Analyzer for Computer Architectures)
Installation
Ensure that Python 3.x, GCC, Valgrind, and OSACA are installed on your system before running the lab.

The lab tests the performance of the following variants of a computational function:

Baseline: Implemented in variant1.py.
Unrolled Variant: Implemented in unrolled.py.
Subexpression elimination Variant: Implemented in subexel.py.
Foil Variant: Implemented in foil.py.
SIMD Variant: Implemented in simd.py.
ILP Variant: Implemented in ilp.py.
Assembly Variant: Implemented in ass.py.
Pipeline(Monica) Variant: Implemented in pipeline.py

Usage
Generating Expressions: Run the gen_expression.py script to generate expressions.

Running Variants: Execute the make command to run all variants. This will compile and execute each variant multiple times, recording execution times in corresponding output files.

Comparing Outputs: Use make compare-files to compare the outputs of the variants with the baseline. This ensures consistency across variants.

Valgrind Analysis: Run make valgrind to perform memory leak analysis using Valgrind on each variant.

OSACA Analysis: Execute make osaca_analysis to analyze each variant using OSACA for cycle counts and identify the best-performing variant.

Then the plotter plots the results.

Cleaning Up: Use make clean to remove generated files and clean up the directory after running the lab.

Running Individual Variants
Each variant can also be executed individually by running its corresponding Python script followed by compiling and executing the generated C code.

Notes
Adjust paths and permissions as necessary in the scripts and Makefile.
Ensure proper cleanup after each run to avoid cluttering the directory.
Results and analysis provided by the lab can be utilized for optimizing the computational function based on performance metrics.
