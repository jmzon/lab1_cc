# test_cases.py

test_cases = [
    {
        "name": "test1",
        "expression": [["a[0]", "a[1]"], ["a[2]", "a[3]"]],
        "expected_output": """
#include <stdio.h>

void compute(float *a, float *o, int n) {
    for (int i = 0; i < n; ++i) {
        o[i] = (a[i + 0] + a[i + 1]) * (a[i + 2] + a[i + 3]);
    }
}
"""
    },
]
