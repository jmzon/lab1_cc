import random
from script import load_product_of_sums, generate_c_function
import subprocess

def run_test(test):
    # generate a random array
    a = [random.randint(0, 100) for _ in range(random.randint(1, 100))]
    # generate a random positive number does it have to be positive?
    n = random.randint(1, 100)
    # call the baseline save it somewhere
    print("Running Test: {}".format(test['name']))
    Baseline_function = load_product_of_sums(test)
    o1 = Baseline_function(a, n)
    # call the script to make a c program for that test
    c_prog = generate_c_function(test)
    subprocess.run(["gcc", "/compute_function.c"])
    # run the c program and save it somehwere
    # substract the first result from the secodn result
    # if it is 0 then we are good, if not the test failed and it shoudl return false and break fromt the original function
    c_function_code = generate_c_function(test['expression'])
    print("Generated C Code:\n", c_function_code)
    print("Expected C Code:\n", test['expected_output'])
    test_passed = c_function_code.strip() == test['expected_output'].strip()
    print("Test Passed:", test_passed)
    print("-----------------------------------")