import os
import matplotlib.pyplot as plt

# Assuming the current directory has the cycle files
cycle_files = [f for f in os.listdir('.') if f.startswith('cycles_')]
variant_names = []
cycle_counts = []

# Read cycle counts from files
for file_name in cycle_files:
    with open(file_name, 'r') as file:
        line = file.readline()
        _, cycles_str = line.split(": ")
        cycles = float(cycles_str.strip())
        variant_names.append(file_name.replace('cycles_', '').replace('.txt', ''))
        cycle_counts.append(cycles)

# Now plot the cycle counts for each variant
plt.figure(figsize=(10, 6))
plt.bar(variant_names, cycle_counts, color='skyblue')
plt.xlabel('Variant')
plt.ylabel('Total Cycles')
plt.title('OSACA Cycle Counts by Variant')
plt.xticks(rotation=45)  # Rotate the variant names to fit them better
plt.tight_layout()  # Adjust the layout so everything fits nicely

# Save the plot as an image file
plt.savefig('osaca_cycle_counts.png')

# Show the plot if needed
# plt.show()
plt.close()  # Close the plot to free memory if not showing it

# Return the path to the saved image file
'/mnt/data/osaca_cycle_counts.png'
