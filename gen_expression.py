import json
import random

def generate_c_function(c_file_path):
    # Initialize the dictionary to hold the expression data
    data = {
        'expression': [
            [],
            []
        ]
    }

    # Generate first list of expressions
    flimit = random.randint(1, 100)
    for i in range(flimit):
        data['expression'][0].append(f"a[{i}]")

    # Generate second list of expressions
    slimit = random.randint(flimit+1, flimit+100)
    for i in range(flimit, slimit):
        data['expression'][1].append(f"a[{i}]")

    # Write the dictionary to a file as JSON
    with open(c_file_path, 'w') as file:
        json.dump(data, file, indent=4)

def main():
    c_file_path = 'expression_gen.json'
    generate_c_function(c_file_path)
    print(f"JSON file generated at {c_file_path}")

if __name__ == "__main__":
    main()
