import json
import random
import os

def generate_test_case(num_sums, max_items_per_sum):

    expression = []
    current_index = 0
    for _ in range(num_sums):
        num_items = random.randint(1, max_items_per_sum)  # Random number of items in each sum
        sum_expression = [f"a[{i}]" for i in range(current_index, current_index + num_items)]
        expression.append(sum_expression)
        current_index += num_items

    return {"expression": expression}

def write_test_case_to_file(file_path, num_sums, max_items_per_sum):
    # Initialize test_cases as a list
    test_cases = []

    # Check if file exists and read existing data
    if os.path.exists(file_path):
        with open(file_path, 'r') as file:
            try:
                data = json.load(file)
                if isinstance(data, list):  # Ensure that data is a list
                    test_cases = data
            except json.JSONDecodeError:
                pass  # If there's a JSON decoding error, keep test_cases as an empty list

    new_test_case = generate_test_case(num_sums, max_items_per_sum)
    test_cases.append(new_test_case)
    with open(file_path, 'w') as file:
        json.dump(test_cases, file, indent=4)


# run the generator
file_path = 'test_cases.json' 
write_test_case_to_file(file_path, random.randint(0, 10), random.randint(1, 10))