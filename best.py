import os

# Retrieve all cycle files from the current directory that start with 'cycles_'
cycle_files = [file for file in os.listdir('.') if file.startswith('cycles_')]

# Initialize variables to store the best variant and its cycle count
best_variant = None
lowest_cycles = float('inf')

# Iterate over each file and extract the cycle count
for file_name in cycle_files:
    # Read the file and extract the cycle count
    with open(file_name, 'r') as file:
        line = file.readline()
        # Extract the cycle count from the line
        _, cycles_str = line.split(": ")
        cycles = float(cycles_str.strip())
        # Check if this is the lowest cycle count found so far
        if cycles < lowest_cycles:
            lowest_cycles = cycles
            # The variant name is the file name without the 'cycles_' prefix and '.txt' suffix
            best_variant = file_name.replace('cycles_', '').replace('.txt', '')

# Output the best variant
print(best_variant)
